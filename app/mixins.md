Mixins
===

CrossChUX contains context-specific mixins that provide properties based on the mapped variables 
and placeholder classes.

For example, as you should already know, colors can be assigned using a placeholder class with the 
following format: 
```
.foo {
  @extend %color--{colorname}-{100|200|300};
}
```
Alternatively, you can use the `color` mixin to achieve a similar result: 
```
.foo {
  @include color(colorname,100|200|300);
}
```

Be aware that the extends and mixins produce different results in your rendered css. Read more 
about the implementation details [here](http://sass-lang.com/guide#topic-6) and 
[here](http://sass-lang.com/guide#topic-7).

## CrossChUX Mixins

### Border

####border-radius($value)  
- **$value**: String name for sizing value.

**returns:** css property/value for `border-radius`




###Colors

####color($name, $value, $important: false)  
Sets the `color` of an element.  

- **$name**: { String } Name for color.  
- **$value**: { Number | `100` | `200` | `300` } Value for color tint.
- **$important** (optional): {Boolean} Add the `!important` keyword to the property assignment.   
  *Default*: `false`

**returns:** css property/value for `color`  


####bg($name, $value, $override: false, $important: false)
Sets the `background-color|background` of an element.  

- **$name**: { String } Name for color.  
- **$value**: { Number | `100` | `200` | `300` } Value for color tint.  
- **$override** (optional): {Boolean} Set `background` instead of `background-color`.     
  *Default*: `false`  
- **$important** (optional): {Boolean} Add the `!important` keyword to the property assignment.     
  *Default*: `false`  

**returns:** css property/value for `background-color` or `background`




###Decoration

####box-shadow($key)  
Set the `box-shadow` of an element, based on keys in `$map--box-shadow`, which uses presets from 
the [Material Design](https://material.angularjs.org/latest/demo/whiteframe) spec. 

- **$key**: { Number } Key for the sizing value. Valid ranges are 1 – 24;  

**returns:** css property/value for `box-shadow`.  




###Dimensions

####width($key)  
Set the `width` of an element, based on keys in `$map--breakpoints`, 
`$map--container-dimensions`, and `$map--grid-dimensions`.  

- **$key**: { String | `min` | `small` | `medium` | `tablet` | `large` | `desktop` | `wide` | 
  `app` | `max` | `10` | `20` | `25` | `30` | `33` | `40` | `50` | `60` | `66` | `70` | `75` | 
  `80` | `90` | `100` | `auto` } Key for the sizing value.  

**returns:** css property/value for `width`.  


####height($key)  
Set the `height` of an element, based on keys in `$map--breakpoints` and 
`$map--container-dimensions`.  

- **$key**: { String | `min` | `small` | `medium` | `tablet` | `large` | `desktop` | `wide` | 
  `app` | `max` | `10` | `20` | `25` | `30` | `33` | `40` | `50` | `60` | `66` | `70` | `75` | 
  `80` | `90` | `100` | `auto`} Key for the sizing value.

**returns:** css property/value for `height`.  


####padding($key: normal, $direction: '')  
Add `padding` to an element.  Based on keys in `$map--container-spacing`.  

- **key** (optional): { String | `xxsm` | `xsm` | `sm` | `normal` | `lg` | `xlg` | `xxlg` | 
`xxxlg` | `none` } Key for the sizing value.  
  *Default*: `normal`  
- **$direction** (optional): {String} Direction for the padding. If no `$direction` is provided, 
  the `padding` value is applied to all sides of the element.  
  *Default*: `''`
  
####margin($key: normal, $direction: '')  
Add `margin` to an element.  Based on keys in `$map--container-spacing`.  

- **key** (optional): { String | `xxsm` | `xsm` | `sm` | `normal` | `lg` | `xlg` | `xxlg` | 
  `xxxlg` | `none` } Key for the sizing value.  
  *Default*: `normal`  
- **$direction** (optional): {String} Direction for the margin. If no `$direction` is provided, 
  the `margin` value is applied to all sides of the element.  
  *Default*: `''`

**returns:** css property/value for `padding` or `padding-(top|right|bottom|left)`. If no key is 
  provided, `normal` is used.
  
  
####grid($key)  
Set the `width` of an element, based on keys in `$map--grid-dimensions`.

  - **$key**: { String | `'1-1'` | `'1-2'` | `'1-3'` | `'1-6'` | `'2-3'` | `'5-6'` } Key for the 
  grid sizing value. These represent ratios, so `'1-1'` equals 100%, `'2-3'` equals 66.666%, etc.
  
**returns:** css property/value for `width`.




###Position

####z-index($key:default)  
Set the `z-index` of an element, based on keys in `$map--z-index`.

- **$key** (optional): { String | `zero` | `under` | `ui` | `default` | `navigation` | `modal` | 
`max` } Key
  for the `z-index` value. If no key is provided, `default` is used.
  *Default*: `default`  

**returns:** css property/value for `z-index`.


####offset($direction, $key: normal)  
Set the `top|right|bottom|left` property of a non-`static` positioned element. Based on keys in 
`$map--container-spacing`.

- **$direction**: {String | `top` | `right` | `bottom` | `left` } Direction property for the 
  offset.  
- **$key** (optional): { String | `xxsm` | `xsm` | `sm` | `normal` | `lg` | `xlg` | `xxlg` | 
  `xxxlg` | `none` } Key for the sizing value. If no key is provided, `normal` is used.  
  *Default*: `normal`  

**returns:** css property/value for `top`, `right`, `bottom`, or `left`. If no key is provided, 
`normal` is used.




###Typography

####font-size($key:normal)  
Set the `font-size` of an element, based on keys in `$map--font-size`.

- **$key** (optional):  String | `xxsm` | `xsm` | `sm` | `normal` | `lg` | `xlg` | `xxlg` | 
`xxxlg`
  } Key for the `font-size` value.  If no key is provided, `normal` is used.  
  *Default*: `normal`  

**returns:** css property/value for `font-size`.


####font-weight($key:normal)  
Set the `font-weight` of an element, based on keys in `$map--font-weight`.

- **$key** (optional): { String | `thin` | `light` | `normal` | `medium` | `bold` } Key for the 
  `font-weight` value. If no key is provided, `normal` is used.  
  *Default*: `normal`  

**returns:** css property/value for `font-weight`.


####font-family($key:sans)  
Set the `font-family` of an element, based on keys in `$map--font-family`.

- **$key**: { String | `sans` } Key for the `font-family` value. If no key is 
  provided, `sans` is used.  
  *Default* (optional): `sans`  

**returns:** css property/value for `font-family`.




###Visibility

####opacity($key:default)  
Set the `opacity` of an element, based on keys in `$map--opacity`.

- **$key**:  String | `full` | `normal` | `muted` | `half` | `disabled` | `none` } Key for the 
  `opacity` value.  If no key is provided, `default` is used.  
  *Default*: `default`  

**returns:** css property/value for `opacity`.




## 3rd-party Mixins
Additionally, CrossChUX contains the following Sass utility mixins:  
- [breakpoint-sass](http://breakpoint-sass.com/)
