const path = require('path');
const _$ = require('underscore.string');

const rootDir = path.join(__dirname, '..');
const pkg = require(path.join(rootDir, 'package.json'));

module.exports = {
  rootDir,
  configDir: path.join(rootDir, 'config'),
  srcDir: path.join(rootDir, 'src'),
  baseDir: path.join(rootDir, 'app'),
  buildDir: path.join(rootDir, 'dist'),
  docsDir: path.join(rootDir, 'docs'),

  host: 'localhost',
  port: 3000,

  app: {
    name: pkg.name,
    title: _$.titleize(pkg.name),
  },
};
