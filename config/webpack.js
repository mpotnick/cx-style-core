const path = require('path');
const webpack = require('webpack');
//const sassImport = require('node-sass-import-once');
const config = require('./config');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  context: config.baseDir,
  entry: [
    `webpack-dev-server/client?http://127.0.0.1:${config.port}`,
    'webpack/hot/only-dev-server',
    './',
  ],
  output: {
    path: config.docsDir,
    publicPath: '',
    filename: './scripts/[name].bundle.[hash].js',
  },
  port: config.port,
  module: {
    preLoaders: [{
      test: /\.(js|jsx)$/,
      include: config.baseDir,
      loader: 'eslint-loader',
    }],
    loaders: [
      // css
      {
        test: /\.css$/,
        loader: 'style!css',
      },
      // babel, ng-annotate
      {
        test: /.*\.js$/,
        exclude: /(node_modules|bower_components)/,
        loaders: ['babel?cacheDirectory=true&presets[]=es2015&presets[]=stage-2', 'ng-annotate'],
      },
      // ng-template
      {
        test: /.*\.html$/,
        exclude: /(node_modules|bower_components)/,
        loaders: ['html'],
      },
      // sass
      {
        test: /\.scss$/,
        loaders: ['style', 'css', 'sass'],
      },
    ],
  },
  devtool: 'eval-source-map',
  devServer: {
    contentBase: './src/',
    historyApiFallback: true,
    //hot: true,
    port: config.port,
    publicPath: '',
    noInfo: false,
  },
  plugins: [
    //new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new HtmlWebpackPlugin(),
    // new BowerWebpackPlugin({
    //   searchResolveModulesDirectories: false
    // }),
  ],
  // plugins: [
  //   new HtmlWebpackPlugin({
  //     title: config.app.title,
  //     hash: false,
  //     cache: true,
  //     template: path.join(config.baseDir, 'index.ejs'),
  //     angular: {
  //       ngApp: config.app.name,
  //     },
  //   }),
  // ],
  sassLoader: {
    includePaths: [path.resolve(config.rootDir, 'node_modules')]
  },
};
